<?php $orig_post = $post;
  global $post;
  $categories = get_the_category($post->ID);
  if ($categories) {
  $category_ids = array();
  foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
  $args=array(
  'category__in' => $category_ids,
  'post__not_in' => array($post->ID),
  'posts_per_page'=> -1, // Number of related posts that will be shown.
  'ignore_sticky_posts'=>1
  );
$my_query = new wp_query( $args );
  if( $my_query->have_posts() ) {
  echo '<div class="related-footer container"><h3>You might also like...</h3><div id="typeset_carousel">';
  while( $my_query->have_posts() ) {
$my_query->the_post();?>
<div class="related-post-thumb">
<div class="related-title">
<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
</div>
<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_post_thumbnail('related_thumb'); ?></a>
</div>
<?php
}
echo '</div></div>';

}
}
$post = $orig_post;
wp_reset_query(); ?>

</div>

<?php if ( $my_query->have_posts() ) { ?>
<div class="carousel-nav">
<a class="fa fa-angle-right" id="prev" href="#"></a>
<a class="fa fa-angle-left" id="next" href="#"></a>
</div>
<?php } ?>