<?php
/**
 * The template for displaying Category Archive pages
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 	Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ): ?>

<?php while ( have_posts() ) : the_post(); ?>

		<?php
			if(!get_post_format()) {
				get_template_part('format', 'standard');
					} else {
					 get_template_part('format', get_post_format());
					};
			?>

<?php endwhile; ?>

<?php if (show_posts_nav()) : ?>

<div class="post-nav-footer">
<div id="post-nav" class="container">

<div class="post-nav-left">
	<?php previous_posts_link(' <i class="icon-angle-left"></i>',''); ?>
</div>

<div class="post-nav-right">
	<?php next_posts_link('<i class="icon-angle-right"></i>',''); ?>
</div>

</div>
</div>
<?php endif; ?>

<?php else: ?>
<div class="container"><h2>No posts to display in <?php echo single_cat_title( '', false ); ?></h2></div>
<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>