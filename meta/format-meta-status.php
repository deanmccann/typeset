<div class="meta-date">
		   
		   <ul class="meta">
		   <?php the_time('M j, Y'); ?> / <a href="<?php comments_link(); ?>"> <?php comments_number( '0 COMMENTS', '1 RESPONSE', '% responses' ); ?></a> / <?php echo getPostViews(get_the_ID()); ?>
		   </ul>
		   
</div>

<div class="post-home-status">
	<a href="<?php echo home_url(); ?>"><i class="fa fa-list"></i></a>
</div>

<div class="post-previous-status">
	<?php previous_post_link('%link', '<i class="fa fa-angle-right"></i>'); ?>

</div>
</div>
		
<div class="meta-content">	   		   
<div class="sidebar-box">
<div class="tagcloud-status">
<?php
if(get_the_tag_list()) {
    echo get_the_tag_list('<p>Tags</p>','','');
}
?>
</div>
</div>
</div>

<div class="page-links">
<?php wp_link_pages(); ?>
</div>