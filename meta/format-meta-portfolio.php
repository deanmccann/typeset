<div class="portfolio-sidebar">
<?php echo preg_replace('/<img[^>]+./','',get_the_content_with_formatting()); ?>
</div>

<div class="post-nav">
<div class="post-next">
	<?php next_post_link('%link', '<i class="fa fa-angle-left"></i>'); ?>
</div>

<div class="post-home">
	<a href="<?php echo home_url(); ?>"><i class="fa fa-list"></i></a>
</div>

<div class="post-previous">
	<?php previous_post_link('%link', '<i class="fa fa-angle-right"></i>'); ?>
</div>
</div>

<div class="page-links">
<?php wp_link_pages(); ?>
</div>