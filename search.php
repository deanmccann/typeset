<?php
/**
 * Search results page
 * 
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 	Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="post-thumbnail">
<a href="<?php the_permalink(); ?>">
<?php

if ( has_post_thumbnail() ) {
	the_post_thumbnail();
}
else {
	echo '<img src="' . get_stylesheet_directory_uri()  . '/images/10.jpg" />';
}
?>
</a>
</div>
		
<div id="page" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <div id="page-title">
			
		    <div class="entry-title">
		   <h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		    </div>
                    
  </div>
		
<div class="entry-page-content">
    <div class="box">
        <?php the_excerpt(); ?>
    </div>
</div>
		
</article>
</div>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>