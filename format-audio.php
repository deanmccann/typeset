<div class="post-thumbnail-audio">
<a href="<?php the_permalink(); ?>">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?></a>
</div>

<div id="posts-audio">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<div class="content-header container">
			<div class="category-audio-tags">
			<?php the_category('&nbsp;'); ?>
			</div>	
</div>

<div class="sub-content-header container">
		
		<div class="entry-title-gallery">
		<div class="box">
		
		<h1><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		</div>
		</div>
		
		<div class="entry-content-quote sixteen columns">
		<div class="box">
		
		<div class="meta-date">
			<ul class="meta">
			
			<?php the_time('M j, Y'); ?> / <a href="<?php comments_link(); ?>"> <?php comments_number( '0 COMMENTS', '1 RESPONSE', '% responses' ); ?></a> / <?php echo getPostViews(get_the_ID()); ?>
			
			</ul>
			</div>

		<?php the_content(); ?>
		
		</div>
		</div>
		
</div>
</article>
</div>