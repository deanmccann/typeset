

	TYPESET

	Created by CourtyardThemes

	- - - - - - - - - - - - - - - - - - - - - - -

	Typeset is a clean, responsive magazine-style blog and portfolio theme.

	For more details, please visit:
	http://www.courtyardthemes.com

	- - - - - - - - - - - - - - - - - - - - - - -
	
	Version 1.2, 02.01.2014
	
	This is a major update which adds a Portfolio Custom Post Type (plugin) for uploading and managing your work.
		
		- CourtyardPortfolio Custom Post Type (lib/plugins/courtyard-portfolio.zip)
		- TGM Class Activation (Auto Plugin Install)
		- New Filterable Portfolio Template
		- Quicksand & Easing jQuery Plugins
		- Updates and improvements to stylesheet performance and maintenance
		- Social Icons Bug Fixes (Dribbble)
		
		*Updated Documentation
	
	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0.2, 30.11.2013
	
	*Performance & Maintenance
		
		-style.css (version number)
		-style.css (reduced default logo font-size via @media queries(max-width: 520px/479pxto support longer logo title and mobile menu alignment
 		-style.css (updated css menu bug display)
	
	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0.1, 19.11.2013
	
	*Performance & Maintenance
		
		-style.css (version number)
		-single.php (updated views bug with setPostViews)
		-style.css (#posts-gallery increased margin-top:-40px - lines 1409)
		-contact-template.php (removed php short tag)
	
	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0 18.11.2013
	
	*First Release	

	Enjoy!

	~ Dean McCann

