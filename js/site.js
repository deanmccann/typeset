/* Superfish --------------------------------------*/
 
 jQuery(document).ready(function($){
        $('ul.sf-menu').superfish({
            delay:             100,
            speed:            'normal',          	 // speed of the animation. Equivalent to second parameter of jQueryÕs .animate() method 
            animation:        {opacity:'show'},  	// fade-in and slide-down animation 
            animationOut:     {opacity:'hide'},
            autoArrows:        true                // disable generation of arrow mark-up 
          
        }); 
    });
 
// Meta Toggle --------------------------------------*/

jQuery(document).ready(function($) { 
		
		$('.meta-content').hide();
		
			$('.fa-angle-down').click(function () {

                 //Change Icon
		$(".fa-angle-down").toggleClass('fa-angle-up');
                
                $('.meta-content').slideToggle(300);
		   
		    return false;
                
		});
});

// Menu Toggle --------------------------------------*/	

jQuery(document).ready(function($) { 		

                $('#sub-header').hide();
			 
                                $('.fa-bars').click(function () {
                
                  //Change Icon
		$(".fa-bars").toggleClass('fa-times');
		
		$('#sub-header').slideToggle(200);
		
		return false;
                
		});
});

// Search Toggle --------------------------------------*/

jQuery(document).ready(function($) {
$('#s').hide();
$("#sbutton").click(function () {
	$('#s').fadeToggle();
        return false;
                });
});

// Flex Slider --------------------------------------*/

jQuery(document).ready(function($) {   
  jQuery('.flexslider').flexslider({
		      animation: "fade",
		      controlNav: false,
		      directionNav: true,
		      randomize: false,
		      touch: true
       });
  
 });

// Related Post CarouFredsel --------------------------------------*/
 jQuery(document).ready(function($) {  
  
	$('#typeset_carousel').carouFredSel({
        circular    : true,
        infinite    : true,
        responsive:true,
        prev: '#prev',
        next: '#next',
        auto: false,
        scroll:1,
        align: 'center',
        swipe: true,
       	   items: {
                width:340,
                height:200,
           		visible: {
           		min: 1,
           		max: 3,
                start: 1
    
           	    }
}
	});
});
 
/* Quicksand --------------------------------------*/
 
jQuery(document).ready(function($){
		
		//Options for the portfolio filter
		var $filterType = $('#sort a').attr('rel');
		var $holder = $('.portfolio-content');
		var $data = $holder.clone();
		
		$('#sort a').click(function(e) {
		$('#sort a').removeClass('active');
			var $filterType = $(this).attr('rel');
			$(this).addClass('active');
			
			if ($filterType == 'all') {
				var $filteredData = $data.find('li');
			}
			else {
				var $filteredData = $data.find('li[data-type~=' + $filterType + ']');
			}
			
			$holder.quicksand($filteredData, {
				duration: 800,
				adjustHeight:"auto",
				easing: 'easeInOutQuad',
	
				}, function() {
				
		  	});
		  
		  return false;
		});
	});