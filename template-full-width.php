<?php
/*
Template Name: Full Width
*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="post-thumbnail">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>
		
<div id="page" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <div id="page-title">
			
		    <div class="entry-title">
		    <h2><?php the_title(); ?></h2>
		    </div>
		    
                   <hr />
                    
  </div>
		
<div class="entry-page-content">
    <div class="box">
        <?php the_content(); ?>
    </div>
</div>
		
</article>
</div>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>