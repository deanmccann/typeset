	
<?php $background = of_get_option('footer_color');
		  if ($background) {
                if ($background['image']) {
                    echo '<footer style="background-size: cover; background-repeat: no-repeat; background:url('.$background['image']. ')">';
                } else {
                    echo '<footer style="background:'.$background['color']. ' ">';
                }	
            } else {
                echo "<footer>";
            }; ?>
	
<div id="footer-widget" class="container">		
		<div class="one-third column">
		<?php dynamic_sidebar( 'widget-01' ); ?>
		</div>
		
		<div class="one-third column">
		<?php dynamic_sidebar( 'widget-02' ); ?>
		</div>
		
		<div class="one-third column">
		<?php dynamic_sidebar( 'widget-03' ); ?>
		</div>
		
</div>
</footer>
	
<div class="sub-footer">
	
	<div class="container">
		<div class="sixteen columns">
			<div class="eight columns">
		<?php if ( function_exists( 'of_get_option' ) ) {
          echo $footer = of_get_option( 'footer_text' );
         }
         ?>	
		</div>
		
		<div id="social-align" class="eight columns">
		 <?php if ( function_exists( 'of_get_option') ) :  

		
                if (of_get_option('facebook_link') != "") :
       ?>
				<a class="fa fa-facebook-square icon-1x social_links" href="<?php echo of_get_option('facebook_link'); ?>"></a>
				
			<?php endif; ?>
			
		<?php if (of_get_option('twitter_link') != "") :
       ?>
				<a class="fa fa-twitter-square icon-1x social_links" href="<?php echo of_get_option('twitter_link'); ?>"></a>
				
				<?php endif; ?>
				
		<?php if (of_get_option('linkedin_link') != "") :
       ?>
				<a class="fa fa-linkedin-sign icon-1x social_links" href="<?php echo of_get_option('linkedin_link'); ?>"></a>
				
		<?php if (of_get_option('pinterest_link') != "") :
       ?>
				<a class="fa fa-pinterest icon-1x social_links" href="<?php echo of_get_option('pinterest_link'); ?>"></a>
				
				<?php endif; ?>
				
		<?php if (of_get_option('dribble_link') != "") :
       ?>
				<a class="fa fa-dribbble icon-1x social_links" href="<?php echo of_get_option('dribble_link'); ?>"></a>
				
				<?php endif; ?>
				
		<?php if (of_get_option('flickr_link') != "") :
       ?>
				<a class="fa fa-flickr icon-1x social_links" href="<?php echo of_get_option('flickr_link'); ?>"></a>
				
				<?php endif; ?>
				
				
		<?php if (of_get_option('vimeo_link') != "") :
       ?>
				<a class="fa fa-vimeo-square icon-1x social_links" href="<?php echo of_get_option('vimeo_link'); ?>"></a>
				
				<?php endif; ?>
				
				
		<?php if (of_get_option('youtube_link') != "") :
       ?>
				<a class="fa fa-youtube-square icon-1x social_links" href="<?php echo of_get_option('youtube_link'); ?>"></a>
				
				<?php endif; ?>
				
				
		<?php if (of_get_option('googleplus_link') != "") :
       ?>
				<a class="fa fa-google-plus-square icon-1x social_links" href="<?php echo of_get_option('googleplus_link'); ?>"></a>
				
				<?php endif; ?>
		
				
				<?php endif; endif?>
	</div>
	</div>
	
</div>
	
