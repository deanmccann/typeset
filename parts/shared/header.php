<header>
	<div class="container">
			
			<div id="logo">
			 <?php if ( function_exists( 'of_get_option') ) :  

                if (of_get_option('logo') != "") :
       ?>
                      
          <a href="<?php echo home_url(); ?>"><img src="<?php echo of_get_option('logo'); ?>" /></a>
               
               <?php else:  ?>
	       
			<a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
			
			<?php endif; endif; ?>
			</div>
		
				
			<span class="menu-toggle"><a class="fa fa-bars" href="#"></a></span>
			
			<div id="sarea">
			<form id="searchform" action="<?php echo home_url(); ?>" method="get" role="search">
			<input id="s" type="text" name="s" value="" placeholder="Search and Press Enter" x-webkit-speech="x-webkit-speech">
			<i id="sbutton" type="submit" value="" class="fa fa-search">&nbsp;</i>
			</form>
			
			</div>
			
	
</div>
</header>

<div id="sub-header">
<div class="container">
	
				<?php wp_nav_menu(
                                              array(
                                              'theme_location'        => 'primary',
					      'container'             => 'div',
					      'container_class'       => 'nav',
					      'menu_class'            => 'sf-menu',
                                              'menu_id'               => 'menu-primary-navigation',
                                              )
                                      ); ?>
			
		

	</div>
</div>