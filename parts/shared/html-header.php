<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><html class="no-js" lang="en"><![endif]-->

<head>

<title><?php bloginfo( 'name' ); ?><?php wp_title( '|' ); ?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head();?>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Google Analytics
================================================== -->

</head>
<body <?php body_class(); ?>>
