<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 	Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
<div id="page" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <div id="page-title">
			
		    <div class="entry-title">
		    <h2>Oops a daisy. The page you are looking for cannot be found.</h2>
		    </div>
                    
  </div>
		
<div class="entry-page-content">
    <div class="box">
        <?php the_content(); ?>
    </div>
</div>
		
</article>
</div>
<?php endwhile; ?>



<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>