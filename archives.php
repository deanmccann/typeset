<?php
/*
Template Name: Archives
*/
?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="post-thumbnail">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>
		
<div id="page" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<aside class="six columns">
			
			<div class="entry-title">
			<h2><?php the_title(); ?></h2>
			</div>
			
			<hr />	
				
</aside>

 <div class="one column">
      &nbsp;
    </div>
		
		<div class="entry-content nine columns">
		<div class="box">
		
		<?php the_content(); ?>
		
	<div class="archive-box">
    <h3 class="archive-title">Last 10 Posts</h3>
    <?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 10, 'format' => 'custom', 'before' => '<li>', 'after' => '</li>' ) ); ?>
	
	 </div>

  <hr />
  
	<div class="archive-box">
    <h3 class="archive-title">Archives by Month</h3>
   <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12, 'format' => 'custom', 'before' => '<li>', 'after' => '</li>' ) ); ?>
	</div>
 
<hr />
  
	<div class="archive-box">
    <h3 class="archive-title">Archives by Subject</h3>
    <?php wp_list_categories( array( 'style' => 'list', 'title_li' => '' ) ); ?>
	</div>
	
		
		</div>
		</div>
		
</article>
</div>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>