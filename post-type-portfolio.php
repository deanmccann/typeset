<?php
/*
Template Name: Portfolio Filterable
*/
?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="post-thumbnail">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>
		
<div id="page" class="container">
   
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <div id="page-title">
			
		    <div class="entry-title">
		    <h2><?php the_title(); ?></h2>
                    
                   <?php the_content(); ?>
		    </div>
		    
                   <hr />
                    
  </div>
  
  <?php 
                  //get gallery categories
                  $taxonomies = array('portfolio_category');
                  
                  $terms = get_terms( $taxonomies );
                  
                  //show filter if categories exist
             if( isset($terms[0]) ) 
                  
                  if($terms[0] !='') { ?>
                  
                  <!-- Portfolio Filter -->

  <div class="entry-page-content">
    <div class="box">
    <div class="sort" id="sort">
      <ul class="filter">
         <li>FILTER:</li>
        <li><a href="#all" rel="all" class="active"><span><?php _e( 'ALL', 'portfolio' ); ?></span></a></li><?php
                    foreach ($terms as $term ) : ?>

        <li><a href="#<?php echo $term->slug; ?>" rel="<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
        <?php endforeach; ?>
      </ul>
    </div><?php } ?>
    </div>
  </div>
  </div>
    </article>

<div class="portfolio-background">
    <div id="portfolio-container" class="container">
    <div class="portfolio-content">
      <?php
                  //get post type ==> portfolio
                 query_posts('post_type=portfolio');
                  ?><?php
                          $count=0;
                  while (have_posts()) : the_post();
                              $count++;
                  //get terms
                  $terms = get_the_terms( $post->ID, 'portfolio_category' );
                              $terms_list = get_the_term_list( $post->ID, 'portfolio_category' );
                  ?><?php if ( has_post_thumbnail() ) {  ?>

      <li data-id="id-<?php echo $count; ?>" data-type="<?php if($terms) { foreach ($terms as $term) { echo $term->slug .' '; } } else { echo 'none'; } ?>" class="portfolio-item">
      <div class="portfolio-post-thumb">
      <div class="portfolio-title">
      <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
      </div>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail('thumbnail-portfolio'); ?>
        </a>
      </div>
            
      </li><?php } ?><?php endwhile; ?>
    </div>
    </div>
  </div>
</div>
  <?php wp_reset_query(); ?>        

<!-- End Container -->

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>