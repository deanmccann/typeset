<?php
/**
 * The Template for displaying single custom post type portfolio
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="post-thumbnail">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>

<div id="portfolio-posts" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <div class="sixteen columns content-header">
			
  </div>

<aside class="six columns">
			
			<div class="portfolio-title">
			<h1 class="portfolio-title"><?php the_title(); ?></h1>
			</div>
			
			 <?php get_template_part( 'meta/format', 'meta-portfolio' ); ?>
</aside>

    <div class="one column">
      &nbsp;
    </div>
		
		<div class="entry-content nine columns">
		<div class="box">
		
  <?php
    $thumb_ID = get_post_thumbnail_id( $post->ID );
    $args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'exclude' => $thumb_ID ); 
    $attachments = get_posts($args);
    if ($attachments) {
        foreach ( $attachments as $attachment ) {
            the_attachment_link( $attachment->ID , true );
        }
    }
    ?>
		
		</div>
		</div>
	      
</article>
</div>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>