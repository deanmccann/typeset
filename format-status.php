<div class="post-thumbnail-status">
<a href="<?php the_permalink(); ?>">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</a>
</div>
<div id="posts-status" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<div class="content-header container">
			<div class="category-status-tags">
			<?php the_category('&nbsp;'); ?>
			</div>	
</div>

<div class="sub-content-header container">
		
		<div class="entry-title-quote">
		<div class="box">
		
		<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		
		<?php the_content(); ?>
		
		</div>
		</div>
		
		<div class="sixteen columns">
		<hr />
		</div>

</div>
</article>
</div>