<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {
	
	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {
	
	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	$options = array();
	
	$options[] = array(   
		"name" => __("Typeset Theme Settings",'typeset'),
		"desc" => __('<p>Control and configure the general setup of your theme. Upload your preferred logo, favicon and completley customize your site with a selection of fonts and color styles.</p>', 'typeset'),
		"type" => "info" );
	
	$options[] = array(   
		"name" => '',
		"desc" => '',
		"type" => "info" );

	// General Settings Section
	$options[] = array(
		'name' => __('General', 'typeset'),
		'type' => 'heading');
	
	// Custom Logo Upload
	$options[] = array(
		'name' => __('Upload Custom Logo', 'typeset'),
		'desc' => __('Upload a logo for your theme.', 'typeset'),
		'id' => 'logo',
		'type' => 'upload');
	
	// Custom Favicon Upload
	$options[] = array(
		'name' => __('Upload Custom Favicon', 'typeset'),
		'desc' => __('Upload a 16px x 16px Png/Gif image that will represent your theme favicon.', 'typeset'),
		'id' => 'favicon',
		'type' => 'upload');
        
        // Footer Text
	$options[] = array(
		'name' => __('Enter Footer Text ', 'typeset'),
		'desc' => __('Add some content to your footer. Leave blank to show nothing.', 'shutter'),
		'id' => 'footer_text',
		'std' => '',
		'type' => 'textarea');
	
	// Google Analytics ID
	$options[] = array(
		'name' => __('Add your Google Analytics ID', 'typeset'),
		'desc' => __('Paste your Google Analytics tracking code here. It will be inserted before the closing body tag of your theme.', 'typeset'),
		'id' => 'google_analytics',
		'std' => '',
		'type' => 'textarea');
	
	// Style & Typography Settings Section
	$options[] = array(
		'name' => __('Style & Typography', 'typeset'),
		'type' => 'heading');
	
	$options[] = array(
		'name' => __('Disable Styles','typeset'),
		'desc' => __('Disable option styles and use theme defaults.', 'typeset'),
		'id' => 'disable_styles',
		'std' => true,
		'type' => 'checkbox' );
	
	$typography_mixed_fonts = array_merge( options_typography_get_os_fonts() , options_typography_get_google_fonts() );
	asort($typography_mixed_fonts);
	
	$options[] = array(
		'name' => __('Plain Text Logo Font', 'typeset'),
		'desc' => __('Select font options for your plain text logo.', 'typeset'),
		'id' => 'google_mixed_title',
		'std' => array( 'size' => '48px', 'face' => 'Lato, serif', 'color' => '#555'),
		'type' => 'typography',
		'options' => array(
			'faces' => $typography_mixed_fonts)
		);
	
	$options[] = array(
		'name' => __('Headings Font', 'typeset'),
		'desc' => __('Choose a font for headings. (h1,h2,h3,h4 etc)', 'typeset'),
		'id' => 'google_mixed_header',
		'std' => array( 'size' => '24px', 'face' => 'Lato, serif', 'color' => '#555', 'style' => 'lighter'),
		'type' => 'typography',
		'options' => array(
			'faces' => $typography_mixed_fonts,
			'style' => false,
			'size' => true)
		);
	
	$options[] = array(
		'name' => __('Body Font', 'typeset'),
		'desc' => __('Select font options for the body text of your theme.', 'typeset'),
		'id' => 'google_mixed_body',
		'std' => array( 'size' => '14px', 'face' => 'Lato, serif', 'color' => '#555', 'style' => 'lighter'),
		'type' => 'typography',
		'style' => 'false',
		'options' => array(
			'faces' => $typography_mixed_fonts,
			'style' => false,
			'size' => true)
		);
        
        $options[] = array(
		'name' => __('Primary Navigation Font', 'typeset'),
		'desc' => __('Select font options for the theme navigation menu.', 'shutter'),
		'id' => 'google_mixed_nav',
		'std' => array( 'size' => '16px', 'face' => 'Georgia, serif', 'color' => '#444'),
		'type' => 'typography',
		'options' => array(
			'faces' => $typography_mixed_fonts,
			'sizes' => false)
		);
        
        $options[] = array(
		'name' => __('Category Tags / Meta Dates Font', 'typeset'),
		'desc' => __('Select font options for the body text of your theme.', 'typeset'),
		'id' => 'google_mixed_meta',
		'std' => array( 'size' => '14px', 'face' => 'Lato, serif', 'color' => '#555', 'style' => 'lighter'),
		'type' => 'typography',
		'style' => 'false',
		'options' => array(
			'faces' => $typography_mixed_fonts,
			'style' => false,
			'size' => false)
		);
		
	$options[] = array(
		'name' => __('Link color', 'typeset'),
		"desc" => __("Select the color for links.", 'typeset'),
		"id" => "link_color",
		"std" => "#444",
		"type" => "color" );
		
	$options[] = array(
		'name' => __('Link hover color', 'typeset'),
		"desc" => __('Select the hover color for links.', 'typeset'),
		"id" => "link_hover_color",
		"std" => "#000",
		"type" => "color" );
	
	$options[] = array(
		'name' => __('Button color', 'typeset'),
		"desc" => __("Select the color for links.", 'typeset'),
		"id" => "button_color",
		"std" => "#444",
		"type" => "color" );
	
	$options[] = array(
		'name' => __('Button hover color', 'typeset'),
		"desc" => __('Select the hover color for links.', 'typeset'),
		"id" => "button_hover_color",
		"std" => "#000",
		"type" => "color" );
        
        // Post Formats Style Settings Section
	$options[] = array(
		'name' => __('Post Format Styles', 'typeset'),
		'type' => 'heading');
	
	$typography_mixed_fonts = array_merge( options_typography_get_os_fonts() , options_typography_get_google_fonts() );
	asort($typography_mixed_fonts);
        
        $options[] = array(
		'name' => __('Standard Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "standard_color",
		"std" => "#444",
		"type" => "color" );
	
       $options[] = array(
		'name' => __('Image Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "image_color",
		"std" => "#444",
		"type" => "color" );
		
         $options[] = array(
		'name' => __('Gallery Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "gallery_color",
		"std" => "#444",
		"type" => "color" );
        
         $options[] = array(
		'name' => __('Video Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "video_color",
		"std" => "#444",
		"type" => "color" );
        
        $options[] = array(
		'name' => __('Audio Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "audio_color",
		"std" => "#444",
		"type" => "color" );
        
         $options[] = array(
		'name' => __('Quote Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "quote_color",
		"std" => "#444",
		"type" => "color" );
        
         $options[] = array(
		'name' => __('Link Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "link_color",
		"std" => "#444",
		"type" => "color" );
        
        $options[] = array(
		'name' => __('Chat Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "chat_color",
		"std" => "#444",
		"type" => "color" );
        
         $options[] = array(
		'name' => __('Aside Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "aside_color",
		"std" => "#444",
		"type" => "color" );
        
         $options[] = array(
		'name' => __('Status Post Accent Color', 'typeset'),
		"desc" => __("Choose an accent color for the post.", 'typeset'),
		"id" => "status_color",
		"std" => "#444",
		"type" => "color" );
        
        // Social Settings Section
	$options[] = array(
		'name' => __('Social Settings', 'typeset'),
		'type' => 'heading');
	
	$options[] = array(
		'name' => __('Facebook Page', 'options_framework_theme'),
		'desc' => __('Add your Facebook address.', 'typeset'),
		'id' => 'facebook_link',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Twitter Account', 'options_framework_theme'),
		'desc' => __('Add your Twitter account.', 'typeset'),
		'id' => 'twitter_link',
		'std' => '',
		'type' => 'text');
        
        $options[] = array(
		'name' => __('Flickr Account', 'options_framework_theme'),
		'desc' => __('Add your Twitter account.', 'typeset'),
		'id' => 'flickr_link',
		'std' => '',
		'type' => 'text');
        
         $options[] = array(
		'name' => __('Vimeo Account', 'options_framework_theme'),
		'desc' => __('Add your Vimeo account.', 'typeset'),
		'id' => 'vimeo_link',
		'std' => '',
		'type' => 'text');
         
          $options[] = array(
		'name' => __('YouTube Account', 'options_framework_theme'),
		'desc' => __('Add your YouTube account.', 'typeset'),
		'id' => 'youtube_link',
		'std' => '',
		'type' => 'text');
          
           $options[] = array(
		'name' => __('Google Plus Account', 'options_framework_theme'),
		'desc' => __('Add your Google Plus account.', 'typeset'),
		'id' => 'googleplus_link',
		'std' => '',
		'type' => 'text');
           
             $options[] = array(
		'name' => __('Dribble Account', 'options_framework_theme'),
		'desc' => __('Add your Dribble account.', 'typeset'),
		'id' => 'dribble_link',
		'std' => '',
		'type' => 'text');
             
               $options[] = array(
		'name' => __('LinkedIn Account', 'options_framework_theme'),
		'desc' => __('Add your LinkedIn account.', 'typeset'),
		'id' => 'linkedin_link',
		'std' => '',
		'type' => 'text');
               
                $options[] = array(
		'name' => __('Pinterest Account', 'options_framework_theme'),
		'desc' => __('Add your Pinterest account.', 'typeset'),
		'id' => 'pinterest_link',
		'std' => '',
		'type' => 'text');
        
	// About / Support Settings Section
	
	$options[] = array(
		'name' => __('About/Support', 'typeset'),
		'type' => 'heading');
		
	
	$options[] = array(
		'name' => __('Theme Credits', 'typeset'),
		'desc' => __('This theme was handcrafted by Dean McCann at <a href="http://www.courtyardthemes.com">Courtyard Themes</a>.', 'typeset'),
		'type' => 'info');
		
	
	return $options;
}

/*
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function($) {

	$('#example_showhidden').click(function() {
  		$('#section-example_text_hidden').fadeToggle(400);
	});

	if ($('#example_showhidden:checked').val() !== undefined) {
		$('#section-example_text_hidden').show();
	}

});
</script>

<?php
}