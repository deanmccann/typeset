<div class="post-thumbnail-quote">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>

<div id="posts-quote">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<div class="content-header container">
			<div class="category-quote-tags">
			<?php the_category('&nbsp;'); ?>
			</div>
</div>
		<div class="sub-content-header container">
		<div class="entry-content-quote sixteen columns">
		<div class="box">
		  
	       <?php get_template_part( 'meta/format', 'meta-quote' ); ?>
		
		<?php remove_filter( 'the_content', 'sharing_display', 19 ); ?>
		<?php remove_filter( 'the_excerpt', 'sharing_display', 19 ); ?>
		<?php the_content(); ?>
	      
		</div>
		
		</div>
		
</article>
</div>
</div>

<div class="comments-footer">
<div class="container">
<div id="comment-post">
<div class="box">

<!-- grab comments on single pages -->
<?php if(is_single ()) { ?>
<?php comments_template( '', true ); ?>
<?php } ?>

</div>
</div>
</div>
</div>

<?php get_template_part( 'related/format', 'related-quote' ); ?>