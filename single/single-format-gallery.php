<div class="post-thumbnail-image">
<?php if ( has_post_format( 'gallery' )) {
                  courtyard_gallery($post->ID, 'thumb-head');
              }
         ?>
</div>

<div id="posts-gallery">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<div class="content-header container">
			<div class="category-gallery-tags">
			<?php the_category('&nbsp;'); ?>
			</div>	
</div>

<div class="sub-content-header container">
		
		<div class="entry-title-gallery">
		<div class="box">
		
		<h1 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		</div>
		</div>
		
		
		<div class="entry-content-quote sixteen columns">
		<div class="box">
		  
		<?php get_template_part( 'meta/format', 'meta-gallery' ); ?>
		
		<?php remove_filter( 'the_content', 'sharing_display', 19 ); ?>
		<?php remove_filter( 'the_excerpt', 'sharing_display', 19 ); ?>
		<?php the_content(); ?>
		
		</div>
		</div>

</article>
</div>
</div>

<div class="comments-footer">
<div class="container">

<div id="comment-post">
<div class="box">

<!-- grab comments on single pages -->
<?php if(is_single ()) { ?>
<?php comments_template( '', true ); ?>
<?php } ?>

</div>
</div>
</div>
</div>

<?php get_template_part( 'related/format', 'related-gallery' ); ?>	