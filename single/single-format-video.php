<figure>
<div class="video-content-single"><?php echo get_post_meta($post->ID, 'video', true) ?></div>
</figure>

<div id="posts-video">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <div class="content-header container">
		    <div class="category-video-tags">
		    <?php the_category(', '); ?>
		     </div>	
  </div>

<div class="sub-content-header container">
			
			<div class="entry-title-video">
			<h1><?php the_title(); ?><span class="toggle"><a class="fa fa-toggle" href="#"></a></span></h1>
			</div>
			
		      <div class="meta-date-center">
		   
		 <?php get_template_part( 'meta/format', 'meta-video' ); ?>
		   
</div>
			      
		<div class="entry-content-video sixteen columns">
		<div class="box">
		<?php remove_filter( 'the_content', 'sharing_display', 19 ); ?>
		<?php remove_filter( 'the_excerpt', 'sharing_display', 19 ); ?>
		<?php the_content(); ?>
	      
		</div>
		</div>
		
</article>
</div>

<div class="comments-footer">
<div class="container">
<div id="comment-post">
<div class="box">

<!-- grab comments on single pages -->
<?php if(is_single ()) { ?>
<?php comments_template( '', true ); ?>
<?php } ?>

</div>
</div>
</div>
</div>

<?php get_template_part( 'related/format', 'related-video' ); ?>