<div class="post-thumbnail">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>

<div id="single-posts" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <div class="sixteen columns content-header">
			<div class="category-tags">
			<?php the_category('&nbsp;'); ?>
			</div>	
  </div>

<aside class="six columns">
			
			<div class="entry-title">
			<h1 class="entry-title">
			<?php the_title(); ?><span class="toggle"><a class="fa fa-angle-down" href="#"></a></span></h1>
			</div>
			
			 <?php get_template_part( 'meta/format', 'meta-standard' ); ?>
</aside>

    <div class="one column">
      &nbsp;
    </div>
		
		<div class="entry-content nine columns">
		<div class="box">
		
		<?php remove_filter( 'the_content', 'sharing_display', 19 ); ?>
		<?php remove_filter( 'the_excerpt', 'sharing_display', 19 ); ?>
		<?php the_content(); ?>
		
		</div>
		</div>
	      
</article>
</div>

<div class="comments-footer">
<div class="container">

<aside class="six columns">
&nbsp;
</aside>

<div class="one column">
&nbsp;
</div>

<div class="nine columns">
<div id="comment-post">
<div class="box">

<!-- grab comments on single pages -->
<?php if(is_single ()) { ?>
<?php comments_template( '', true ); ?>
<?php } ?>

</div>
</div>
</div>
</div>
</div>

<?php get_template_part( 'related/format', 'related-standard' ); ?>