<div class="post-thumbnail-image">
<a href="<?php the_permalink(); ?>">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?></a>
</div>

<div id="posts-image" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<div class="sixteen columns content-header">
			<div class="category-image-tags">
			<?php the_category('&nbsp;'); ?>
			</div>	
</div>

<aside class="six columns">
			
			<div class="entry-title">
			<h1><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			</div>
			
			<div class="meta-date">
			<ul class="meta">
		  
			<?php the_time('M j, Y'); ?> / <a href="<?php comments_link(); ?>"> <?php comments_number( '0 COMMENTS', '1 RESPONSE', '% responses' ); ?></a> / <?php echo getPostViews(get_the_ID()); ?>
			
			</ul>
			</div>

</aside>

   <div class="spacer one column">
      &nbsp;
    </div>
		
		<div class="entry-content nine columns">
		<div class="box">
		
		<?php the_excerpt(); ?>
		
		<a class="more-link" href="<?php echo get_permalink(); ?>">Continue Reading</a> <span class="spacer"> or </span>
					
		<a href="http://www.instapaper.com/hello2?url=<?php echo get_permalink(); ?> &amp;title=<?php echo the_title(); ?>" title="Read '<?php the_title_attribute(); ?>' later with Instapaper" class="read-later">Read Later</a>
		
		</div>
		</div>

</article>
</div>