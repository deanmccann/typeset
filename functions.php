<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 	Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once dirname( __FILE__ ) .'/external/starkers-utilities.php';
	require_once dirname( __FILE__ ) .'/external/class-tgm-plugin-activation.php';
	
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/inc/options-framework.php';
	}

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	if ( !function_exists( 'courtyard_theme_setup' ) ) {
    function courtyard_theme_setup() {
	
	//text domain for localiation settings
	load_theme_textdomain( 'typeset', get_template_directory() . '/lang' );
	
	// Add support fior thumbnail sizes
	add_theme_support('post-thumbnails');
	add_image_size( 'related_thumb', 300, 180, true );
	add_image_size( 'thumbnail-portfolio', 200, 200, true );
	
	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	
	// Add support for post formats.
	  add_theme_support( 
            'post-formats', 
            array(
                'aside',
                'gallery',
		'link',
                'image',
		'quote',
		'status',
                'video',
                'audio',
		'chat'
            ) 
        );
    }
}

add_action( 'after_setup_theme', 'courtyard_theme_setup' );
	
register_nav_menus(array('primary' => 'Primary Navigation'));
	
add_action( 'widgets_init', 'courtyard_register_sidebars' );
		
	function courtyard_register_sidebars() {
			
	// Repeat register_sidebar() code for additional sidebars.
	
	// Register the 'primary' sidebar.
		
	// Repeat register_sidebar() code for additional sidebars.
	
	register_sidebar(
			array(
				'id' => 'widget-01',
				'name' => __( 'Footer Widget Left', 'typeset' ),
				'description' => __( 'Footer Widget Left', 'typeset' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h4 class="widget-title">',
				'after_title' => '</h4>'
			)
		);
	
	register_sidebar(
			array(
				'id' => 'widget-02',
				'name' => __( 'Footer Widget Center', 'typeset' ),
				'description' => __( 'Footer Widget Center', 'typeset' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h4 class="widget-title">',
				'after_title' => '</h4>'
			)
		);
	
	register_sidebar(
			array(
				'id' => 'widget-03',
				'name' => __( 'Footer Widget Right', 'typeset' ),
				'description' => __( 'Footer Widget Right', 'typeset' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h4 class="widget-title">',
				'after_title' => '</h4>'
			)
		);
	}
	
	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */
	
	// remove add formatting to get_the_content
	
	function get_the_content_with_formatting () {
		$content = get_the_content();
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
		}
	
	// add google analytics support
	
	function __analytics_head()
		{
		if (of_get_option('google_analytics') != "") : 
		    ?>
			<script type="text/javascript">
			    var _gaq = _gaq || [];
			    _gaq.push(['_setAccount', '<?php echo esc_js( of_get_option('google_analytics') ) ; ?>']);
			    _gaq.push(['_trackPageview']);
			    (function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			    })();
			</script>
		    
		    <?php endif;
	}
	
	add_action( 'wp_head', '__analytics_head', 100 );
	
	//include options framework style actions	
	include( 'lib/options_framework_actions.php' );
	
	//include tgm required plugin	
	include( 'lib/tgm_register_plugin.php' );
	
	// enable threaded comments
	function enable_threaded_comments(){
		if (!is_admin()) {
			if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
				wp_enqueue_script('comment-reply');
			}
	}
	add_action('get_header', 'enable_threaded_comments');
	
	
	// post navigation conditional

	function show_posts_nav() {
		global $wp_query;
			return ($wp_query->max_num_pages > 1);
	}

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );
	add_action( 'tgmpa_register', 'typeset_register_required_plugins' );
	
	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );
	
	// add support for custom excerpt length and more
	if ( ! isset( $content_width ) ) $content_width = 940;
	
	function custom_excerpt_length( $length ) {
		return 75;
	}
	
	add_filter( 'excerpt_length', 'custom_excerpt_length', 55 );
	
	function new_excerpt_more( $more ) {
		  global $post;
	return '...&nbsp;';
	}
	
	add_filter('excerpt_more', 'new_excerpt_more');

	
	// remove junk from head
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	
	remove_filter( 'the_content', 'sharing_display',19);
	remove_filter( 'the_excerpt', 'sharing_display',19);

	
	//remove css margins from admin bar
	
	function my_admin_bar_init() {
	remove_action('wp_head', '_admin_bar_bump_cb');
	}
	add_action('admin_bar_init', 'my_admin_bar_init');

	//get post views

// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
function posts_column_views($defaults){
    $defaults['post_views'] = __('Views', 'typeset');
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
    if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}

// function to add styles to WP editor.

add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );

function my_mce_before_init( $settings ) {

    $style_formats = array(
        array(
    		'title' => 'Indent Image',
    		'block' => 'div',
    		'classes' => 'inset'
    	),
	
	  array(
    		'title' => 'Blockquote',
    		'block' => 'blockquote',
		'wrapper' => true
    	)
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}

add_action( 'admin_init', 'add_my_editor_style' );

function add_my_editor_style() {
	add_editor_style();
}


	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */
	
// trigger fitvids

	function __fitvids_script() { ?> 
	<script type="text/javascript">

	jQuery(function($){
	   $("body").fitVids();
	 });
       
	</script>
		       <?php }
		
	add_action( 'wp_footer', '__fitvids_script', 100 );
	

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_register_script( 'fitvids', get_template_directory_uri().'/js/jquery.fitvids.js', array( 'jquery' ), ''. true );
		wp_register_script( 'superfish', get_template_directory_uri().'/js/superfish.js', array( 'jquery' ), '', true );
		wp_register_script( 'hoverintent', get_template_directory_uri().'/js/hoverIntent.js', array( 'jquery' ), '', true );
		wp_register_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '', true );
		wp_register_script( 'caroufredsel', get_template_directory_uri() . '/js/jquery.carouFredSel-6.2.1-packed.js', array('jquery'), '', true );
		wp_register_script( 'quicksand', get_template_directory_uri() . '/js/jquery.quicksand.js', array('jquery'), '', true );
		wp_register_script( 'easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array('jquery'), '', true );
		
		wp_enqueue_script( 'site' );
		wp_enqueue_script( 'fitvids' );
		wp_enqueue_script( 'superfish' );
		wp_enqueue_script( 'hoverintent' );
		wp_enqueue_script( 'flexslider' );
		wp_enqueue_script( 'caroufredsel' );
		wp_enqueue_script( 'quicksand' );
		wp_enqueue_script( 'easing' );

		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
		wp_register_style( 'base', get_template_directory_uri().'/css/base.css', '', '', 'all' );
		wp_register_style( 'skeleton', get_template_directory_uri().'/css/fluid-skeleton.css', '', '', 'all' );
		wp_register_style( 'fontawesome', get_template_directory_uri().'/css/font-awesome.min.css', '', '', 'all' );
		wp_register_style( 'superfish', get_template_directory_uri().'/css/superfish.css', '', '', 'all' );
		wp_register_style( 'flexslider', get_template_directory_uri().'/css/flexslider.css', '', '', 'all' );
		
		wp_enqueue_style( 'screen' );
		wp_enqueue_style( 'base' );
		wp_enqueue_style( 'skeleton' );
		wp_enqueue_style( 'fontawesome' );
		wp_enqueue_style( 'superfish' );
		wp_enqueue_style( 'flexslider' );
		wp_enqueue_style( 'googlefonts-titillum', 'http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700');
		wp_enqueue_style( 'googlefonts-sourcesanspro', 'http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700');
		
		}

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>

		<li>
			<article id="comment-<?php comment_ID() ?>">
			<div class="comment-author">
				<?php comment_author_link() ?><br />
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				</div>
		
				<div class="comment-text">
				<?php comment_text() ?>
	
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div>
				
				<hr />
				
			</article>
		</li>
	
		<?php endif;
		
	}
	
/* ========================================================================================================================
	
	Slideshows

======================================================================================================================== */
	
	// Initialize Slider
	
function courtyard_slider_start() { ?>
	<script type="text/javascript">
		$(window).load(function() {
		  $('#main-slider').flexslider({
		      animation: "slide",
		      controlNav: false,
		      directionNav: true,
		      randomize: true,
		      touch: true
		  });
	      });
	</script>
	<?php }
	
add_action( 'wp_head', 'courtyard_slider_start' );

// Create Slider

// Native Gallery Slideshows

if ( !function_exists( 'courtyard_gallery' ) ) {
    function courtyard_gallery($postid, $imagesize) { ?>
        <script type="text/javascript">
    		jQuery(document).ready(function($){
    		 // Slideshow
			 $('.flexslider').flexslider();
			animation: "fade",
		        controlNav: false,
		        directionNav: true,
		        randomize: true,
		        touch: true
 });
		
    	</script>
    <?php 
        $thumbid = 0;
    
        // get the featured image for the post
        if( has_post_thumbnail($postid) ) {
            $thumbid = get_post_thumbnail_id($postid);
        }
        echo "<!-- BEGIN #slider-$postid -->\n<div id='slider-$postid' class='flexslider'>";
        
        $posttitle = the_title_attribute( array( 'echo' => 0 ) );
        
        // get all of the attachments for the post
        $args = array(
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'attachment',
            'post_parent' => $postid,
            'post_mime_type' => 'image',
            'post_status' => null,
            'numberposts' => -1,
        );
        $attachments = get_posts($args);
        if( !empty($attachments) ) {
            echo '<ul class="slides">';
            $i = 0;
            foreach( $attachments as $attachment ) {
                if( $attachment->ID == $thumbid ) continue;
                $src = wp_get_attachment_image_src( $attachment->ID, $imagesize );
                $caption = $attachment->post_excerpt;
                $caption = ($caption) ? $caption : $posttitle;
                $alt = ( !empty($attachment->post_content) ) ? $attachment->post_content : $attachment->post_title;
                echo "<li><img height='$src[2]' width='$src[1]' src='$src[0]' alt='$alt' title='$caption' class='slider-border' /></li>";
                $i++;
            }
            echo '</ul>';
        }
        echo "<!-- END #slider-$postid -->\n</div>";
    }
}