<?php

/*
 * The CSS file selected in the options panel 'stylesheet' option
 * is loaded on the theme front end
 */
	 
function options_stylesheets_alt_style()   {
if ( of_get_option('stylesheet') ) {
			wp_enqueue_style( 'options_stylesheets_alt_style', of_get_option('stylesheet'), array(), null );
		}
	}
	
add_action( 'wp_enqueue_scripts', 'options_stylesheets_alt_style' );


/**
 * Returns an array of system fonts
 * Feel free to edit this, update the font fallbacks, etc.
 */

function options_typography_get_os_fonts() {
	// OS Font Defaults
	$os_faces = array(
		'Arial, sans-serif' => 'Arial',
		'Garamond, "Hoefler Text", Times New Roman, Times, serif' => 'Garamond',
		'Georgia, serif' => 'Georgia',
		'"Helvetica Neue", Helvetica, sans-serif' => 'Helvetica Neue',
		'Tahoma, Geneva, sans-serif' => 'Tahoma',
	);
	return $os_faces;
}

/**
 * Returns a select list of Google fonts
 * Feel free to edit this, update the fallbacks, etc.
 */

function options_typography_get_google_fonts() {
	// Google Font Defaults
	$google_faces = array(
		'Abril Fatface, cursive' => 'Abril Fatface',
		'Arvo, serif' => 'Arvo',
		'Crimson Text, serif' => 'Crimson',
		'Droid Sans, sans-serif' => 'Droid Sans',
		'Doppio One, sans-serif' => 'Doppio One',
		'EB Garamond, serif' => 'EB Garamond',
		'Lato, serif' => 'Lato',
		'Lobster, cursive' => 'Lobster',
		'Montserrat, sans-serif' => 'Montserrat',
		'Josefin Slab, serif' => 'Josefin Slab',
		'Open Sans, sans-serif' => 'Open Sans',
		'Old Standard TT, serif' => 'Old Standard TT',
		'Pacifico, cursive' => 'Pacifico',
		'Rokkitt, serif' => 'Rokkit',
		'Raleway, cursive' => 'Raleway',
		'Source Sans Pro, sans-serif' => 'Source Sans Pro',
                'Titillium Web, sans-serif' => 'Titillium Web',
		'Ubuntu, sans-serif' => 'Ubuntu',
		'Vollkorn, serif' => 'Vollkorn',
	);
	return $google_faces;
}

/* 
 * Outputs the selected option panel styles inline into the <head>
 */
 
function options_typography_styles() {
 	
 	// It's helpful to include an option to disable styles.  If this is selected
 	// no inline styles will be outputted into the <head>
 	
 		if ( !of_get_option( 'disable_styles' ) ) {
		$output = '';
		$input = '';
		
		if ( of_get_option( 'google_mixed_title' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_title' ) , '#logo a, #logo a:hover');
		}
		
		if ( of_get_option( 'google_mixed_header' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_header' ) , 'h1,h2,h3,h4,h5,h6, .entry-title, .entry-title-gallery, .entry-title-quote, .entry-title-video, entry-title-chat, blockquote, blockquote p, .archive-title, .entry-404');
		}
		
		if ( of_get_option( 'google_mixed_body' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_body' ) , 'body, .tagline, .more-link a, .read-later a, li.cat-item, ul.filter a, ul.filter a:hover, ul.filter a.active, .button, button, input[type="submit"], input[type="reset"], input[type="button"], .comment-author, #reply-title, label, nav');
		}
		
		if ( of_get_option( 'google_mixed_nav' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_nav' ) , '.sf-menu, .sf-menu a');
		}
		
		if ( of_get_option( 'google_mixed_meta' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_meta' ) , '.meta, .tags-title, div.sharedaddy .sd-title, .category-tags a, .category-image-tags a, .category-video-tags a');
		}
		
		if ( of_get_option( 'link_color' ) ) {
			$output .= 'a, a:visited, .entry-title, .entry-title a, .menu-toggle a, a.more-link, a.read-later {color:' . of_get_option( 'link_color' ) . '}';
		}
		
		if ( of_get_option( 'link_hover_color' ) ) {
			$output .= 'a:hover, .entry-title a:hover, .menu-toggle a:hover, a.more-link:hover, a.read-later:hover, #footer-widget a:hover {color:' . of_get_option( 'link_hover_color' ) . '}';
		}
		
		if ( of_get_option( 'button_color' ) ) {
			$output .= '#contactForm #submit, .tagcloud a, #searchsubmit, #commentform #submit, .comment-reply-link, .post-nav-left a, .post-nav-right a {background-color:' . of_get_option( 'button_color' ) . '}';
		}
		
		if ( of_get_option( 'button_hover_color' ) ) {
			$output .= '#contactForm #submit:hover, .tagcloud a:hover, #searchsubmit:hover, #commentform #submit:hover, .comment-reply-link:hover, .post-nav-left a:hover, .post-nav-right a:hover {background-color:' . of_get_option( 'button_hover_color' ) . '}';
		}
		
		if ( of_get_option( 'standard_color' ) ) {
			$output .= '#posts, #single-posts, .related-post-thumb img, .related-post-thumb img:hover {border-top-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.related-post-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-tags a, .post-next a, .post-home a, .post-previous a, .tagcloud-standard a, .related-title a , .carousel-nav a {background-color:' . of_get_option( 'standard_color' ) . '}';
		}
		
		if ( of_get_option( 'image_color' ) ) {
			$output .= '#posts-image, .related-image-thumb img {border-top-color:' . of_get_option( 'image_color' ) . '}';
			$output .= '.related-image-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-image-tags a, .post-next-image a, .post-home-image a, .post-previous-image a, .tagcloud-image a, .carousel-image-nav a {background-color:' . of_get_option( 'image_color' ) . '}';
		}
		
		if ( of_get_option( 'gallery_color' ) ) {
			$output .= '#posts-gallery, .related-gallery-thumb img {border-top-color:' . of_get_option( 'gallery_color' ) . '}';
			$output .= '.related-gallery-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-gallery-tags a, .post-next-gallery a, .post-home-gallery a, .post-previous-gallery a, .tagcloud-gallery a, .carousel-gallery-nav a {background-color:' . of_get_option( 'gallery_color' ) . '}';
		}
		
		if ( of_get_option( 'video_color' ) ) {
			$output .= '#posts-video, .related-video-thumb {border-top-color:' . of_get_option( 'video_color' ) . '}';
			$output .= '.related-video-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-video-tags a, .post-next-video a, .post-home-video a, .post-previous-video a, .tagcloud-video a,.carousel-video-nav a {background-color:' . of_get_option( 'video_color' ) . '}';
		}
		
		if ( of_get_option( 'audio_color' ) ) {
			$output .= '#posts-audio, .related-audio-thumb {border-top-color:' . of_get_option( 'audio_color' ) . '}';
			$output .= '.related-audio-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-audio-tags a, .post-next-audio a, .post-home-audio a, .post-previous-audio a, .tagcloud-audio a, .carousel-audio-nav a{background-color:' . of_get_option( 'audio_color' ) . '}';
		}
		
		if ( of_get_option( 'quote_color' ) ) {
			$output .= '#posts-quote, .related-quote-thumb {border-top-color:' . of_get_option( 'quote_color' ) . '}';
			$output .= '.related-quote-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-quote-tags a, .post-next-quote a, .post-home-quote a, .post-previous-quote a, .tagcloud-quote a, .carousel-quote-nav a {background-color:' . of_get_option( 'quote_color' ) . '}';
		}

		if ( of_get_option( 'link_color' ) ) {
			$output .= '#posts-link, .related-link-thumb {border-top-color:' . of_get_option( 'link_color' ) . '}';
			$output .= '.related-link-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-link-tags a, .post-next-link a, .post-home-link a, .post-previous-link a, .tagcloud-link a, .carousel-link-nav a {background-color:' . of_get_option( 'link_color' ) . '}';
		}
		
		if ( of_get_option( 'chat_color' ) ) {
			$output .= '#posts-chat, .related-chat-thumb {border-top-color:' . of_get_option( 'chat_color' ) . '}';
			$output .= '.related-chat-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-chat-tags a, .post-next-chat a, .post-home-chat a, .post-previous-chat a, .tagcloud-chat a, .carousel-chat-nav a {background-color:' . of_get_option( 'chat_color' ) . '}';
		}
		
		if ( of_get_option( 'aside_color' ) ) {
			$output .= '#posts-aside, .related-aside-thumb {border-top-color:' . of_get_option( 'aside_color' ) . '}';
			$output .= '.related-aside-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-aside-tags a, .post-next-aside a, .post-home-aside a, .post-previous-aside a, .tagcloud-aside a, .carousel-aside-nav a {background-color:' . of_get_option( 'aside_color' ) . '}';
		}
		
		if ( of_get_option( 'status_color' ) ) {
			$output .= '#posts-status, .related-status-thumb {border-top-color:' . of_get_option( 'status_color' ) . '}';
			$output .= '.related-status-thumb img:hover {border-bottom-color:' . of_get_option( 'standard_color' ) . '}';
			$output .= '.category-status-tags a, .post-next-status a, .post-home-status a, .post-previous-status a, .tagcloud-status a, .carousel-status-nav a {background-color:' . of_get_option( 'status_color' ) . '}';
		}
		
		if ( $output != '' ) {
			$output = "\n<style>\n" . $output . "</style>\n";
			echo $output;
		}
	}
}

add_action('wp_head', 'options_typography_styles');

/* 
 * Returns a typography option in a format that can be outputted as inline CSS
 */
 
function options_typography_font_styles($option, $selectors) {
		$output = $selectors . ' {';
		$output .= ' color:' . $option['color'] .'; ';
		$output .= 'font-family:' . $option['face'] . '; ';
		$output .= 'font-weight:' . $option['style'] . '; ';
		$output .= 'font-size:' . $option['size'] . '; ';
		$output .= '}';
		$output .= "\n";
		return $output;
}

/**
 * Checks font options to see if a Google font is selected.
 * If so, options_typography_enqueue_google_font is called to enqueue the font.
 * Ensures that each Google font is only enqueued once.
 */
 
if ( !function_exists( 'options_typography_google_fonts' ) ) {
	function options_typography_google_fonts() {
		$all_google_fonts = array_keys( options_typography_get_google_fonts() );
		// Define all the options that possibly have a unique Google font
                $google_font = of_get_option('google_mixed_title', false);
		$google_mixed = of_get_option('google_mixed_header', false);
		$google_mixed_2 = of_get_option('google_mixed_body', false);
		$google_mixed_3 = of_get_option('google_mixed_meta', false);
		// Get the font face for each option and put it in an array
		$selected_fonts = array(
			$google_font['face'],
			$google_mixed['face'],
			$google_mixed_2['face'],
			$google_mixed_2['face']);
		// Remove any duplicates in the list
		$selected_fonts = array_unique($selected_fonts);
		// Check each of the unique fonts against the defined Google fonts
		// If it is a Google font, go ahead and call the function to enqueue it
		foreach ( $selected_fonts as $font ) {
			if ( in_array( $font, $all_google_fonts ) ) {
				options_typography_enqueue_google_font($font);
			}
		}
	}
}

add_action( 'wp_enqueue_scripts', 'options_typography_google_fonts' );

/**
 * Enqueues the Google $font that is passed
 */
 
function options_typography_enqueue_google_font($font) {
	$font = explode(',', $font);
	$font = $font[0];
	// Certain Google fonts need slight tweaks in order to load properly
	// Like our friend "Raleway"
	if ( $font == 'Raleway' )
		$font = 'Raleway:100';
	$font = str_replace(" ", "+", $font);
	wp_enqueue_style( "options_typography_$font", "http://fonts.googleapis.com/css?family=$font", false, null, 'all' );
}
?>