<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 	Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="post-thumbnail">
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
</div>
		
<div id="page" class="container">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<aside class="six columns">
			
			<div class="entry-title">
			<h2><?php the_title(); ?></h2>
			</div>
			
			<hr />	
</aside>

 <div class="one column">
      &nbsp;
    </div>
		
		<div class="entry-content nine columns">
		<div class="box">
		
		<?php the_content(); ?>
		
		</div>
		</div>
		
</article>
</div>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>