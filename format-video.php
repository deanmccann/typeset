<figure>
<div class="video-content"><?php echo get_post_meta($post->ID, 'video', true) ?></div>
</a>
</figure>

<div id="posts-video">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

<div class="content-header container">
			<div class="category-video-tags">
			<?php the_category('&nbsp;'); ?>
			</div>	
</div>

<div class="sub-content-header container">
			
			<div class="entry-title-video">
			<div class="box">
			<h1><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			</div>
			</div>
			
			<div class="meta-date-center">
			<ul class="meta">
			
			<?php the_time('M j, Y'); ?> / <a href="<?php comments_link(); ?>"> <?php comments_number( '0 COMMENTS', '1 RESPONSE', '% responses' ); ?></a> / <?php echo getPostViews(get_the_ID()); ?>
			
			</ul>
			</div>
			
		
		<div class="entry-content-video sixteen columns">
		<div class="box">
		
		<?php the_excerpt(); ?>
		
		<a class="more-link" href="<?php echo get_permalink(); ?>">Continue Reading</a> <span class="spacer"> or </span>
					
		<a href="http://www.instapaper.com/hello2?url=<?php echo get_permalink(); ?> &amp;title=<?php echo the_title(); ?>" title="Read '<?php the_title_attribute(); ?>' later with Instapaper" class="read-later">Read Later</a>

	      
		</div>
		</div>

</div>
</article>
</div>
